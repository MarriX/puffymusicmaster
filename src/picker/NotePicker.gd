extends Spatial

onready var red := preload("res://assets/mat/red_picker_mat.tres")
onready var yellow := preload("res://assets/mat/yellow_picker_mat.tres")
onready var green := preload("res://assets/mat/green_picker_mat.tres")

export(int, 1,3) var line
var is_pressed := false

func _ready() -> void:
	set_material()
	set_process_input(true)

func set_material() -> void:
	match line:
		1:
			$MeshInstance.material_override = green 
		2:
			$MeshInstance.material_override = yellow
		3:
			$MeshInstance.material_override = red

func _input(event: InputEvent) -> void:
	match line:
		1:
			if event.is_action_pressed("ui_left"):
				is_pressed = true
			elif event.is_action_released("ui_left"):
				is_pressed = false
		2:
			if event.is_action_pressed("ui_down"):
				is_pressed = true
			elif event.is_action_released("ui_down"):
				is_pressed = false
		3:
			if event.is_action_pressed("ui_right"):
				is_pressed = true
			elif event.is_action_released("ui_right"):
				is_pressed = false

func _process(_delta: float) -> void:
	if is_pressed:
		self.scale = Vector3(0.9, 0.9, 0.9)
	else:
		self.scale = Vector3(1, 1, 1)

