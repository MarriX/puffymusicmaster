extends Spatial

onready var note_scene = preload("res://src/note/note.tscn")
onready var spawn = $Spawn

var notes_data = [
	{
		"pos" : 0
	},
	{
		"pos" : 400
	},
	{
		"pos" : 800
	},
	{
		"pos" : 1200
	}
]

var speed = Vector3(0, 0, 1)
var note_scale = 0.005

func _ready() -> void:
	add_note()

func add_note() -> void:
	for note_data in notes_data:
		randomize()
		var note = note_scene.instance()
		note.line = randi()%3+1
		note.position = int(note_data.pos)*note_scale
		spawn.add_child(note)

func _process(delta: float) -> void:
	spawn.translate(speed*delta)
