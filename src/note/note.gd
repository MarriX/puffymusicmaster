extends Spatial

onready var red := preload("res://assets/mat/red_note_mat.tres")
onready var yellow := preload("res://assets/mat/yellow_note_mat.tres")
onready var green := preload("res://assets/mat/green_note_mat.tres")


export(int, 1,3) var line
var position = 0
var collected = false
var is_colliding = false
var picker 

func _ready() -> void:
	set_material()
	set_position()

func set_material() -> void:
	match line:
		1:
			$MeshInstance.material_override = green 
		2:
			$MeshInstance.material_override = yellow
		3:
			$MeshInstance.material_override = red

func set_position() -> void:
	var x 
	match line:
		1:
			x = -1
		2:
			x = 0
		3: 
			x = 1
	self.translation = Vector3(x, 0, -position)

func _process(delta: float) -> void:
	if not collected:
		if picker and is_colliding:
			if picker.is_pressed:
				collected = true
				picker.is_pressed = false
				hide()

func _on_Area_area_entered(area: Area) -> void:
	if area.is_in_group("picker"):
		is_colliding = true
		picker = area.get_parent()


func _on_Area_area_exited(area: Area) -> void:
	if area.is_in_group("picker"):
		is_colliding = false
